<?php

class Bootstrap {
	
	
	public function getUrl(){}
	

	public function __construct() {
		$url=isset($_GET["url"])? $_GET["url"] : null ;
		
		if ($url!=null) {
			$url=rtrim($url,"/");
		
			$url=explode("/",$url);
		}
		else{
		
			unset($url);
		}
		
		if (isset($url[0])) {
		
			$fileName="app/controllers/".$url[0].".php";
			
			if(file_exists($fileName))
			{
				include $fileName;
				
				
				if (class_exists($url[0])) {
					$c=new $url[0]();
					
					if(isset($url[2])){
						
						if(method_exists($c, $url[1]))
						{
							$c->$url[1]($url[2]);
						}
						else{
							
							echo "Controller içinde method bulunamadı.";
							
						}
						
						
					
					}else{
							
						if(isset($url[1])){

							if(method_exists($c, $url[1]))
							{
								$c->$url[1]();
							}
							else{
									
								echo "Controller içinde method bulunamadı.";
									
							}
							
						}
						else{
							
						if(method_exists($c, "index"))
							{
								$c->index();
							}
							else{
									
								echo "Controller içinde method bulunamadı.";
									
							}
						}
				}
				}
				else {
					echo "Class dosyası bulunamadı.";
				}
				
								
			}
			else {
				
				echo "Controller dosyası bulunamadı.";
			}
		}
		else{
		
			include "app/controllers/index.php";
		
			$c=new Index();
		
			$c->anasayfa();
		
		}		
	}
}

