<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class index extends Controller {

	
	public static $index_model;
	
    public function anasayfa() {
            
        $this->load->view("anasayfa");
        
    }
    
    public function index(){
    
    
    	$this->login();
    
    }
    
    public function __construct() {
        
        parent::__construct();
        $this::$index_model=$this->load->model("index_model");
        
    }
    
    public function isimListele($id) {
        //
        $data["isimListele"]=$index_model->isimListele($id);
        
        $this->load->view("isimListele",$data);
    }
    
    public function makaleKaydet(){
    	
    	$baslik=$_POST["baslik"];
    	$icerik=$_POST["icerik"];
    	$etiket=$_POST["etiket"];
    	
        $data=array(
          "baslik"=>$baslik,
          "icerik"=>$icerik,  
          "etiket"=>$etiket  
        );

        $result=$index_model->makaleKaydet($data);
        
        if ($result) {
        	
        	$data["mesaj"]=array(
        			 
        			"mesaj"=>"Kayıt işlemi başarılı."
        	);
        }
        else{
        	
        	$data["mesaj"]=array(
        			
        			"mesaj"=>"Kayıt işlemi yapılırken bir sorun oluştu."
        	);
        }
        
        $this->load->view("yenimakale",$data);
    }
    
    public function yeniMakale(){
    	
    	$this->load->view("yenimakale");
    }
    
    public function guncelle() {
    	
    	$index_model->guncelle();
    }
    
    public function delete() {
    	
    	$index_model->delete();
    }
    
    public function makaleListele() {
    	
    	$data["makaleListele"]=$index_model->makaleListele();
    	
    	$this->load->view("makaleListele",$data);
    }
    
    function makaleGuncelle($param) {
    	;
    }
    function makaleSil($param) {
    	;
    }
    
}
