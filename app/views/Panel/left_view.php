<div id="content">
		<div id="sidebar">
			<div class="box">
				<div class="h_title">&#8250; Main control</div>
				<ul id="home">
					<li class="b1"><a class="icon view_page" href="<?php echo SITE_URL;?>" target="_blank">Siteyi Görüntüle</a></li>
					<li class="b2"><a class="icon report" href="">Raporlar</a></li>
					<li class="b1"><a class="icon add_page" href="">Yeni İçerik Ekle</a></li>
					<li class="b2"><a class="icon config" href="">Site Ayarları</a></li>
				</ul>
			</div>
			
			<div class="box">
				<div class="h_title">&#8250; Manage content</div>
				<ul>
					<li class="b1"><a class="icon page" href="">Tüm Yazılar</a></li>
					<li class="b2"><a class="icon add_page" href="">Yeni İçerik Ekle</a></li>
					<li class="b1"><a class="icon photo" href="">Add new gallery</a></li>
					<li class="b2"><a class="icon category" href="">Kategoriler</a></li>
					<li class="b2"><a class="icon add_page" href="">Yeni Kategori Ekle</a></li>
				</ul>
			</div>
			<div class="box">
				<div class="h_title">&#8250; Kullanıcılar</div>
				<ul>
					<li class="b1"><a class="icon users" href="">Tüm Kullanıcılar</a></li>
					<li class="b2"><a class="icon add_user" href="">Yeni Kullanıcı Ekle</a></li>
					<li class="b1"><a class="icon block_users" href="">Engellenen Kullanıcılar</a></li>
				</ul>
			</div>
			<div class="box">
				<div class="h_title">&#8250; Settings</div>
				<ul>
					<li class="b1"><a class="icon config" href="">Site configuration</a></li>
					<li class="b2"><a class="icon contact" href="">Contact Form</a></li>
				</ul>
			</div>
		</div>